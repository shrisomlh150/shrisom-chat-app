require('dotenv').config()
var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);

/**Socket  */
var io = require('socket.io').listen(server);

server.listen(process.env.PORT, () => {
    console.log(`Server is running on port ${process.env.PORT}`)
})

let onlineUser = [];

io.on('connection', (socket) => {
    console.log('Socket is connected.')

    // on submiting the username
    socket.on('submit', (data) => {
        onlineUser.push({
            user: data.user
        })

        io.emit('onLineUser', onlineUser);
    })

    socket.on('disconnect', () => {
        io.emit('onLineUser', onlineUser);
    })

    /**On sending invites to user */
    socket.on('invite', (data) => {
        socket.join(data.room)
        io.emit('newInvite', data)
    })

    /**On accepting the invite */
    socket.on('join', function(data) {
        //joining
        console.log(data.user + 'joined the room : ' + data.room);

        io.emit('inviteAccepted', data);
        socket.join(data.room);
    });

    /**On Submiting new message */
    socket.on('message', function(data) {
        io.in(data.room).emit('incomingMessageListner', { user: data.user, message: data.message, room: data.room });
    })

    /**On Leave the room */
    socket.on('leave', function(data) {

        console.log(data.user + 'left the room : ' + data.room);

        socket.broadcast.to(data.room).emit('onLeaveListner', { user: data.user, message: 'has left this room.', room: data.room });

        socket.leave(data.room);
    });
})