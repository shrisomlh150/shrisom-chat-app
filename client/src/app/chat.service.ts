import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private socket = io(environment.server_url);

  constructor() { }

  submit(data) {
    this.socket.emit('submit', data);
  }

  getOnlineUser() {
    let observable = new Observable<any>(observer => {
      this.socket.on('onLineUser', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  invite(data) {
    this.socket.emit('invite', data)
  }

  newInvite() {
    let observable = new Observable<any>(observer => {
      this.socket.on('newInvite', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  accept(data) {
    this.socket.emit('join', data)
  }

  onAcceptListner() {

    let observable = new Observable<any>(observer => {
      this.socket.on('inviteAccepted', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  onMessage(data) {
    this.socket.emit('message', data);
  }

  onMessageListner() {

    let observable = new Observable<any>(observer => {
      this.socket.on('incomingMessageListner', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  leave(data) {
    this.socket.emit('leave', data);
  }

  onLeaveListner() {

    let observable = new Observable<any>(observer => {
      this.socket.on('onLeaveListner', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }
}
