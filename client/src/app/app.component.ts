import { Component, OnInit } from '@angular/core';
import { ChatService } from './chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'client';
  currentUser: string;
  onlineUser = [];
  invitationList = [];
  chatBox = [];
  message: string;

  constructor(private _chatService: ChatService) {

  }

  ngOnInit(): void {

    this.currentUser = localStorage.getItem('currentUser')

    /**Get list of online users */
    this._chatService.getOnlineUser().subscribe(data => {
      this.onlineUser = data;
      console.log('Online User', this.onlineUser)
    })

    /**Get all invite list */
    this._chatService.newInvite().subscribe(data => {
      console.log('Invite list', data)
      if (data.to == this.currentUser)
        this.invitationList.push(data);
    })

    /**Create chat box */
    this._chatService.onAcceptListner().subscribe(data => {
      console.log('Accepted', data)
      if (data.user == this.currentUser || data.to == this.currentUser)
        this.createChatBox(data);
    })

    /**Get Message of room */
    this._chatService.onMessageListner().subscribe(data => {
      console.log('New MEssage', data)
      this.pushMessage(data)
    })

    /**On Opponet leave room */
    this._chatService.onLeaveListner().subscribe(data => {
      console.log('New MEssage', data)
      this.pushMessage(data)
    })
  }

  submit() {
    console.log('Current User', this.currentUser);
    localStorage.setItem('currentUser', this.currentUser);
    this._chatService.submit({ user: this.currentUser });
  }

  invite(user) {
    console.log(user);
    this._chatService.invite({
      user: this.currentUser,
      to: user.user,
      room: `${this.currentUser}_${user.user}`
    })
  }

  accept(index) {
    this._chatService.accept(this.invitationList[index]);
    this.invitationList.splice(index, 1);
  }

  createChatBox(data) {
    this.chatBox.push({
      id: data.room,
      opponent: data.user == this.currentUser ? data.to : data.user,
      user: this.currentUser,
      message: []
    })
  }

  pushMessage(data) {
    let index = this.chatBox.findIndex(x => x.id == data.room)

    this.chatBox[index].message.push({
      user: data.user,
      message: data.message
    })

    console.log('Chat Box', this.chatBox)
  }

  onMessage(room) {
    this._chatService.onMessage({ user: room.user, room: room.id, message: this.message })
  }

  leave(room) {
    this._chatService.leave({ user: this.currentUser, room: room.id })

    this.chatBox.splice(this.chatBox.findIndex(x => x.id == room.id), 1)

  }
}
